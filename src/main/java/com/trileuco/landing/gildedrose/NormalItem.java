package com.trileuco.landing.gildedrose;

public class NormalItem extends ShopItem {

    public NormalItem(Item item) {
        super(item);
    }

    @Override
    public void updateItem() {
        reduceSellIn();
        reduceQuality();

        if (getSellIn() < ENDED_SELLIN) {
            reduceQuality();
        }
    }

}
