package com.trileuco.landing.gildedrose;

class GildedRose {

    private static final String BRIE_NAME = "Aged Brie";
    private static final String TICKETS_NAME = "Backstage passes to a TAFKAL80ETC concert";
    private static final String SULFURAS_NAME = "Sulfuras, Hand of Ragnaros";
    private static final String CONJURED_NAME = "Conjured";

    private Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        for (Item item : items) {
            ShopItem customItem = convertToCustomItem(item);
            customItem.updateItem();
        }
    }

    private ShopItem convertToCustomItem(Item item) {

        if (item.name.equals(BRIE_NAME)) {
            return new Brie(item);
        }

        if (item.name.equals(TICKETS_NAME)) {
            return new Ticket(item);
        }

        if (item.name.equals(SULFURAS_NAME)) {
            return new Sulfuras(item);
        }

        if (item.name.contains(CONJURED_NAME)) {
            return new Conjured(item);
        }

        return new NormalItem(item);
    }
}