package com.trileuco.landing.gildedrose;

public class Conjured extends ShopItem {

    public Conjured(Item item) {
        super(item);
    }

    @Override
    public void updateItem() {
        reduceSellIn();

        reduceQuality();
        reduceQuality();

        if (getSellIn() < ENDED_SELLIN) {
            reduceQuality();
            reduceQuality();
        }
    }

}
