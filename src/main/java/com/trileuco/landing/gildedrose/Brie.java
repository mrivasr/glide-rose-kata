package com.trileuco.landing.gildedrose;

public class Brie extends ShopItem {

    public Brie(Item item) {
        super(item);
    }

    @Override
    public void updateItem() {
        reduceSellIn();

        increaseQuality();

        if (getSellIn() < ENDED_SELLIN) {
            increaseQuality();
        }
    }
}
