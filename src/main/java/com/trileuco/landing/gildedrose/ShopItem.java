package com.trileuco.landing.gildedrose;

public abstract class ShopItem extends Item {

    private static final int TOP_QUALITY = 50;
    private static final int MIN_QUALITY = 0;
    static final int ENDED_SELLIN = 0;

    private Item item;

    public ShopItem(Item item) {
        super(item.name, item.sellIn, item.quality);
        this.item = item;
    }

    public abstract void updateItem();

    int getSellIn() {
        return item.sellIn;
    }

    void increaseQuality() {
        if (item.quality < TOP_QUALITY) {
            item.quality += 1;
        }
    }

    void reduceQuality() {
        if (item.quality > MIN_QUALITY) {
            item.quality -= 1;
        }
    }

    void setQualityToZero() {
        item.quality = 0;
    }

    void reduceSellIn() {
        item.sellIn--;
    }

}
