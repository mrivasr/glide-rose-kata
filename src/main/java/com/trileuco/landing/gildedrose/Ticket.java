package com.trileuco.landing.gildedrose;

public class Ticket extends ShopItem {

    private static final int DOUBLE_QUALITY_BARRIER = 10;
    private static final int TRIPLE_QUALITY_BARRIER = 5;

    public Ticket(Item item) {
        super(item);
    }

    @Override
    public void updateItem() {
        reduceSellIn();
        increaseQuality();

        if (getSellIn() < DOUBLE_QUALITY_BARRIER) {
            increaseQuality();
        }

        if (getSellIn() < TRIPLE_QUALITY_BARRIER) {
            increaseQuality();
        }

        if (getSellIn() < ENDED_SELLIN) {
            setQualityToZero();
        }
    }
}
